#!/usr/bin/env bash

# Git
mkdir -p ${HOME}/.config/git
ln -sf ${HOME}/git/dotfiles/git/config ${HOME}/.config/git/config
ln -sf ${HOME}/git/dotfiles/git/ignore ${HOME}/.config/git/ignore

# Htop
mkdir -p ${HOME}/.config/htop
ln -sf ${HOME}/git/dotfiles/htop/htoprc ${HOME}/.config/htop/htoprc

# Kitty
mkdir -p ${HOME}/.config/kitty
ln -sf ${HOME}/git/dotfiles/kitty/kitty.conf ${HOME}/.config/kitty/kitty.conf

# Starship
mkdir -p ${HOME}/.config
ln -sf ${HOME}/git/dotfiles/starship/starship.toml ${HOME}/.config/starship.toml

# Vim
mkdir -p ${HOME}/.vim
ln -sf ${HOME}/git/dotfiles/vim/vimrc ${HOME}/.vimrc
rm ${HOME}/.vim/pack
ln -s ${HOME}/git/dotfiles/vim/pack ${HOME}/.vim/pack

# Yamllint
mkdir -p ${HOME}/.config/yamllint
ln -sf ${HOME}/git/dotfiles/yamllint/config ${HOME}/.config/yamllint/config

# Zsh
mkdir -p ${HOME}/.zsh
ln -sf ${HOME}/git/dotfiles/zsh/zshrc ${HOME}/.zshrc
ln -sf ${HOME}/git/dotfiles/zsh/zshenv ${HOME}/.zshenv
ln -sf ${HOME}/git/dotfiles/zsh/zlogin ${HOME}/.zlogin
ln -sf ${HOME}/git/dotfiles/zsh/zlogout ${HOME}/.zlogout
rm ${HOME}/.zsh/plugins
ln -s ${HOME}/git/dotfiles/zsh/plugins ${HOME}/.zsh/plugins

# Darwin
if [[ "$OSTYPE" == darwin* ]]; then

  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  brew tap homebrew/bundle
  brew bundle install --file ${HOME}/git/dotfiles/bundles/default --no-lock

  chsh -s /usr/local/bin/zsh

  read -p "Is this a work setup? (Yes/No): " confirm
  if [[ $confirm == [yY][eE][sS] ]]; then
    brew bundle install --file ${HOME}/git/dotfiles/bundles/work --no-lock
  else
    brew bundle install --file ${HOME}/git/dotfiles/bundles/play --no-lock
  fi

fi
