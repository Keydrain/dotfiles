{
  description = "Keydrain's dotfiles";
  outputs = { self }: {
    nixosModules = {
      git = ({
        home.file = {
          gitconfig = {
            target = ".config/git/config";
            source = git/config;
          };
          gitignore = {
            target = ".config/git/ignore";
            source = git/ignore;
          };
        };
      });
      htop = ({
        home.file.htoprc = {
          target = ".config/htop/htoprc";
          source = htop/htoprc;
        };
      });
      kitty = ({
        home.file.kitty-conf = {
          target = ".config/kitty/kitty.conf";
          source = kitty/kitty.conf;
        };
      });
      starship = {
        home.file.starship-toml = {
          target = ".config/starship.toml";
          source = starship/starship.toml;
        };
      };
      vim = ({
        programs.vim.extraConfig = (builtins.readFile vim/vimrc);
        home.file.vim-pack = {
          target = ".vim/pack";
          source = vim/pack;
        };
      });
      yamllint = ({
        home.file.yamllint-config = {
          target = ".config/yamllint/config";
          source = yamllint/config;
        };
      });
      zsh = ({
        programs.zsh = {
          envExtra = (builtins.readFile zsh/zshenv);
          initExtra = (builtins.readFile zsh/zshrc);
          loginExtra = (builtins.readFile zsh/zlogin);
          logoutExtra = (builtins.readFile zsh/zlogout);
        };
        home.file.zsh-plugins = {
          target = ".zsh/plugins";
          source = zsh/plugins;
        };
      });
    };
  };
}
