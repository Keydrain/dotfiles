# Keydrain's Dotfiles

Repo to track custom configuration files such as .zshrc and .vimrc.

## Submodules

This repo uses submodules:
* For initial clone:
  ```
  git clone --recurse-submodules -j8 git@gitlab.com:Keydrain/dotfiles.git
  ```
* For existing updates:
  ```
  git submodule update --init --recursive --remote
  ```

## Setup

### Darwin / Linux

Run `./setup.sh`

### Nixos

Checkout https://gitlab.com/Keydrain/nixconfig for more information

## Adding new plugins

```
git submodule add https://github.com/Author/Plugin.git
```
